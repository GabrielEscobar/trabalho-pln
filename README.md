# Trabalho feito para a disciplina de Processamento de Linguagem Natural da Universidade Federal de Goiás (UFG)

## Abordagem de vizinhos próximos para a descoberta de hiperônimos

#### Aluno: Gabriel Escobar Paes
#### Professora: Nádia Félix

Arquivos `notebook` e `python` utilizados no trabalho foram colocados tudo nessa pasta porque antes estava junto com meu repositório e tinha vários arquivos que não foram utilizados no trabalho.
Essa troca de diretório é notado nos arquivos, eles listam os diretórios antigos.

Não será possível executar esse trabalho a não ser que mude os diretórios de leitura de arquivos nos `.ipyb` e `.py`.

Também não foi anexado o corpus e o modelo word2vec por serem grandes.

Esse trabalho utilizou 3 computadores diferentes, o meu notebook pessoal, um computador de um amigo (dá pra ver isso no arquivo scripts/make_files.py) e um servidor do meu orientador (usado para storage e principalmente para o treinamento do modelo).


Os arquivos de saída gerados como resposta para o trabalho estão em `/sem-eval/test/output/`.

* 1C.spanish.output.txt - arquivo de resposta utilizando o modelo com window=10
* 1C.spanish.outputW5.txt - arquivo de resposta utilizando o modelo com window=5


As métricas foram obtidas pelo próprio script do SemEval2018 em *sem-eval/task9-scorer.py*

> python task9-scorer.py ./test/gold/1C.spanish.test.gold.txt ./test/output/1C.spanish.output.txt