import os
from gensim.models import Word2Vec
from gensim.test.utils import common_texts, get_tmpfile, datapath
class MySentences(object):
    def __init__(self, dirname):
        self.dirname = dirname

    def __iter__(self):
        for fname in os.listdir(self.dirname):
            for line in open(os.path.join(self.dirname, fname), mode="r", encoding="utf8"):
                yield line.split()

print("Iniciando a leitura dos arquivos...")
sentences = MySentences('/dpgs-data/gabrielescobar/files') # a memory-friendly iterator
path = get_tmpfile('/dpgs-data/gabrielescobar/word2vec.model')

print("Iniciando treinamento...")
model = Word2Vec(sentences, size=300, workers=4, sg=1, window=10)
#model = Word2Vec(sentences, size=300, workers=4, sg=1, window=5)
model.save("/dpgs-data/gabrielescobar/word2vec.model")
print ("Modelo Salvo")

